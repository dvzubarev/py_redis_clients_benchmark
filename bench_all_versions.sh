#!/usr/bin/env bash


variants=(coredis231 coredis231_wo_hiredis
          coredis303 coredis351 coredis385 coredis3115
          coredis402 coredis421 coredis431 coredis460 coredis460_wo_ext)


for var in ${variants[@]}; do
    nix --quiet shell .#python.pkgs.bench_"$var" -c ./bench.sh
done
