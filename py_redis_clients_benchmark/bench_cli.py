#!/usr/bin/env python3

import argparse
import logging
import asyncio


import uvloop
import coredis

from py_redis_clients_benchmark import coredis4_bench
from py_redis_clients_benchmark import coredis3_bench
from py_redis_clients_benchmark import coredis2_bench

async def run(opts):
    coredis_major_version = int(coredis._version.get_versions()['version'].split('.')[0])
    if coredis_major_version == 2:
        await coredis2_bench.run(opts)
    elif coredis_major_version == 3:
        await coredis3_bench.run(opts)
    else:
        await coredis4_bench.run(opts)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--verbose", "-v", action="store_true", default=False)
    parser.add_argument("--redis_port", "-p", default=7777, type=int)

    subparsers = parser.add_subparsers(help='sub-command help')

    run_parser = subparsers.add_parser('run', help='help of run')
    run_parser.add_argument("--dont_use_xadd", "-nx", dest='use_xadd', default=True, action='store_false')
    run_parser.add_argument("--atomic", "-a", default=False, action='store_true')
    run_parser.add_argument("--batch_num", "-b", default=80, type=int)
    run_parser.add_argument("--batch_size", "-s", default=1000, type=int)
    run_parser.set_defaults(func=run)

    args = parser.parse_args()

    FORMAT = "%(asctime)s %(levelname)s: %(name)s: %(message)s"
    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO, format=FORMAT)
    try:
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        asyncio.run(args.func(args))
    except Exception as e:
        logging.exception("failed to run: %s ", e)
