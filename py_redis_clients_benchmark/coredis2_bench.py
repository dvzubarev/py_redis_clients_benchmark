#!/usr/bin/env python3

import coredis
from py_redis_clients_benchmark.common import  add_and_read
from py_redis_clients_benchmark.coredis_common import QNAME, coredis_add_bench_common

async def _exec_xadd(pipe, queue_name, rec):
    await pipe.execute_command(
        'XADD',
        queue_name,
        b'*',
        *rec
    )

async def coredis_add_bench(opts, redis, rank, batch_cnt):
    await coredis_add_bench_common(opts, redis, rank, batch_cnt, _exec_xadd)




async def coredis_read_bench(opts, redis, ttl_cnt):
    group_name = b"workers"
    try:
        await redis.execute_command('XGROUP CREATE', QNAME, group_name, b'0', b'MKSTREAM')
    except coredis.exceptions.ResponseError as e:
        if not str(e) == 'BUSYGROUP Consumer Group name already exists':
            raise

    read = 0
    resps = []
    while read < ttl_cnt:

        resp = await redis.execute_command(
            'XREADGROUP',
            b'GROUP',
            group_name,
            b'client-1',
            b'BLOCK',
            b'5000',
            b'COUNT',
            b'%d' % opts.batch_size,
            b'NOACK',
            b'STREAMS',
            QNAME,
            '>',
        )
        resp = resp[QNAME]
        read += len(resp)

        for stream_id, d in resp:
            resps.append(int(d[b'n']))


    assert sum(range(ttl_cnt)) == sum(resps), f"bad assert {sum(resps)} = {sum(range(ttl_cnt))}"


async def cleanup(opts, redis, *args):
    await redis.delete(QNAME)

async def run(opts):

    redis = coredis.StrictRedis(host='127.0.0.1', port=opts.redis_port)
    redis_version  = 'redis-' + (await redis.info())['redis_version']
    coredis_version = 'coredis-' + coredis._version.get_versions()['version']

    await add_and_read(opts, redis, coredis_add_bench, coredis_read_bench, cleanup,
                       (redis_version, coredis_version))
