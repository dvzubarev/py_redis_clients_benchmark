#!/usr/bin/env python3

import coredis
from py_redis_clients_benchmark.common import  add_and_read
from py_redis_clients_benchmark.coredis_common import QNAME, coredis_add_bench_common

def _cmd(c):
    if int(coredis._version.get_versions()['version'].split('.')[1]) >= 8:
        return c.encode('ascii')
    return c
XADD_CMD = _cmd('XADD')

async def _exec_xadd(pipe, queue_name, rec):
    await pipe.execute_command(
        XADD_CMD,
        queue_name,
        b'*',
        *rec
    )

async def coredis_add_bench(opts, redis, rank, batch_cnt):
    await coredis_add_bench_common(opts, redis, rank, batch_cnt, _exec_xadd)



async def coredis_read_bench(opts, redis, ttl_cnt):
    group_name = b"workers"
    try:
        await redis.execute_command(_cmd('XGROUP CREATE'), QNAME, group_name, b'0', b'MKSTREAM')
    except Exception as e:
        if not str(e) in ('BUSYGROUP Consumer Group name already exists', 'Consumer Group name already exists'):
            raise

    read = 0
    resps = []
    cmd = _cmd('XREADGROUP')
    while read < ttl_cnt:
        resp = await redis.execute_command(
            cmd,
            b'GROUP',
            group_name,
            b'client-1',
            b'BLOCK',
            b'5000',
            b'COUNT',
            b'%d' % opts.batch_size,
            b'NOACK',
            b'STREAMS',
            QNAME,
            '>',
        )
        if resp is None:
            print(read, ttl_cnt)
            break

        if isinstance(resp, list) and len(resp) == 1:
            resp = resp[0]
        if isinstance(resp, dict):
            resp = resp[QNAME]
        elif isinstance(resp, list):
            if len(resp) == 1:
                resp = resp[0]
            _, resp = resp

        read += len(resp)

        if isinstance(resp[0][1], dict):
            for stream_id, d in resp:
                resps.append(int(d[b'n']))
        else:
            for _, l in resp:
                for i in range(0, len(l),2):
                    if l[i] == b'n':
                        resps.append(int(l[i+1]))
                        break


    assert sum(range(ttl_cnt)) == sum(resps), f"bad assert {sum(resps)} = {sum(range(ttl_cnt))}"


async def cleanup(opts, redis, *args):
    await redis.delete(QNAME)

async def run(opts):

    redis = coredis.StrictRedis(host='127.0.0.1', port=opts.redis_port)
    redis_version  = 'redis-' + (await redis.info())['redis_version']
    coredis_version = 'coredis-' + coredis._version.get_versions()['version']

    await add_and_read(opts, redis, coredis_add_bench, coredis_read_bench, cleanup,
                       (redis_version, coredis_version))
