#!/usr/bin/env python3

import time
import asyncio


def gen_records(cur_batch_num, batch_size, as_tuple=False):
    for i in range(batch_size):
        cur_num = batch_size * cur_batch_num + i
        nf = b'%d' % cur_num
        af = str(cur_num).zfill(24).encode('ascii')
        cf = b'12345678'
        sf = b'span18l28lpnyfnwuh9h29h9hshsrh9srht9rs' + i.to_bytes(4, 'little')

        if not as_tuple:
            yield {
                b'n': nf,
                #fake payload
                b'a': af,
                b'c': cf,
                b's': sf
            }
        else:
            yield (
                b'n', nf,
                b'a', af,
                b'cf', cf,
                b'sf', sf
            )



async def do_bench(coro, *args, coro_cleanup, **kwargs):
    #make warmup
    await coro(*args, **kwargs)
    await coro_cleanup(*args, **kwargs)
    times = []
    results = []
    for _ in range(3):
        start = time.time()
        res = await coro(*args, **kwargs)
        end = time.time()
        times.append(end - start)
        results.append(res)
        await coro_cleanup(*args, **kwargs)

    return results, times

def _print(name, versions, times):
    for i, t in enumerate(times):
        print(f'{i},{",".join(versions)},{name},{t:.4}')

async def simple_add(opts, redis, coro, coro_cleanup, versions):

    opts.atomic = False
    _, times = await do_bench(coro, opts, redis, coro_cleanup=coro_cleanup)
    _print('add_pipe', versions, times)

    opts.atomic = True
    _, times =  await do_bench(coro, opts, redis, coro_cleanup=coro_cleanup)
    _print('add_atomic_pipe', versions, times)


async def _run_add_and_read(opts, redis, coro_add, coro_read):
    batch_cnt = max(opts.batch_num // 4, 1)
    await asyncio.gather(
        coro_read(opts, redis, opts.batch_size * 4 * batch_cnt),
        coro_add(opts, redis, 0, batch_cnt),
        coro_add(opts, redis, 1, batch_cnt),
        coro_add(opts, redis, 2, batch_cnt),
        coro_add(opts, redis, 3, batch_cnt)
    )

async def add_and_read(opts, redis, coro_add, coro_read, coro_cleanup, versions):
    opts.atomic = False

    _, times = await do_bench(_run_add_and_read, opts, redis, coro_add, coro_read,
                              coro_cleanup=coro_cleanup)

    _print('rw_pipe', versions, times)

    opts.atomic = True
    _, times = await do_bench(_run_add_and_read, opts, redis, coro_add, coro_read,
                              coro_cleanup=coro_cleanup)
    _print('rw_atomic_pipe', versions, times)
