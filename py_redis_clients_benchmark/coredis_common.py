#!/usr/bin/env python3


from py_redis_clients_benchmark.common import  gen_records

QNAME = b'coredis_stream'

async def _exec_xadd(pipe, queue_name, rec):
    await pipe.execute_command(
        b'XADD',
        queue_name,
        b'*',
        *rec
    )

async def _call_xadd(pipe, queue_name, rec):
    await pipe.xadd(queue_name, rec)



async def coredis_add_bench_common(opts, redis, rank, batch_cnt, exec_xadd=None):
    gen_as_tuple = not opts.use_xadd
    if opts.use_xadd:
        exec_coro = _call_xadd
    else:
        exec_coro = _exec_xadd if exec_xadd is None else exec_xadd

    for b in range(batch_cnt):
        b = rank * batch_cnt + b
        async with await redis.pipeline(transaction=opts.atomic) as pipe:
            for rec in gen_records(b, opts.batch_size, as_tuple=gen_as_tuple):
                await exec_coro(pipe, QNAME, rec)

            res = await pipe.execute()
            for r in res:
                assert r
