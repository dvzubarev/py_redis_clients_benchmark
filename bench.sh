#!/usr/bin/env bash

cleanup() {
   kill $REDIS_PID
}

#start redis server
# echo "starting redis"
# redis-server --version
rm -rf temp-redis && mkdir -p temp-redis
redis-server --port 7777 --dir ./temp-redis &> redis.log &
REDIS_PID=$!

trap cleanup EXIT


#testing
# py-redis-clients-benchmark run -b 1 -s 3

# profile
# python -m cProfile -o man-prof-4 /tmp/nix-shell.Bx5rM6/tmp.8PYnh2y86B/bin/py-redis-clients-benchmark run

py-redis-clients-benchmark run -nx
