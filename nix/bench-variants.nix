{src}:
final: prev: (
  builtins.mapAttrs(name: value:
    final.callPackage ./default.nix (value // {src=src;})
  ) {
    bench_coredis231 = {
      coredis=final.coredis231;
    };
    bench_coredis231_wo_hiredis = {
      coredis=final.coredis231_wo_hiredis;
    };

    bench_coredis303 = {
      coredis=final.coredis303;
    };

    bench_coredis351 = {
      coredis=final.coredis351;
    };

    bench_coredis385 = {
      coredis=final.coredis385;
    };

    bench_coredis3115 = {
      coredis=final.coredis3115;
    };

    bench_coredis402 = {
      coredis=final.coredis402;
    };

    bench_coredis421 = {
      coredis=final.coredis421;
    };

    bench_coredis431 = {
      coredis=final.coredis431;
    };

    bench_coredis460 = {
      coredis=final.coredis460;
    };
    bench_coredis460_wo_ext = {
      coredis=final.coredis460_wo_ext;
    };

  }
)
