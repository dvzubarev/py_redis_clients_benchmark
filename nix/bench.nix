{
  src,
  lib,
  buildPythonPackage,
  uvloop,
  coredis,
}:

buildPythonPackage {
  pname = "py-redis-clients-benchmark";
  version = "0.0.1";
  inherit src;


  buildInputs = [];
  propagatedBuildInputs=[
    uvloop
    coredis
  ];

}
