{v, hash, build-native-ext ? true}:
{ buildPythonPackage, deprecated, fetchPypi, lib, packaging, typing-extensions,
  pympler,
  wrapt,
  hiredis,

  build-native-ext_ ? build-native-ext,
  mypy,
  beartype,
  types-deprecated
}:

buildPythonPackage rec {
  pname = "coredis";
  version = v;

  src = fetchPypi {
    inherit pname version;
    sha256 = hash;
  };

  buildInputs = [] ++ lib.optionals build-native-ext_
    [mypy beartype types-deprecated ];

  propagatedBuildInputs = [ deprecated typing-extensions packaging pympler wrapt hiredis];

  doCheck = false;

  meta = with lib; {
    description = "Python async client for Redis key-value store";
    homepage = "https://github.com/alisaifee/coredis";
  };
}
