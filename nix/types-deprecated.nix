# WARNING: This file was automatically generated. You should avoid editing it.
# If you run pynixify again, the file will be either overwritten or
# deleted, and you will lose the changes you made to it.

{ buildPythonPackage, fetchPypi, lib }:

buildPythonPackage rec {
  pname = "types-deprecated";
  version = "1.2.9";

  src = fetchPypi {
    inherit version;
    pname = "types-Deprecated";
    sha256 = "1ymn1cwm0b9ij252myk8rns64chj423w7p4ikqsnb62h564yak70";
  };

  # TODO FIXME
  doCheck = false;

  meta = with lib; {
    description = "Typing stubs for Deprecated";
    homepage = "https://github.com/python/typeshed";
  };
}
