{v, hash, build-native-ext ? true}:
{
  buildPythonPackage,
  async-timeout,
  deprecated,
  fetchPypi,
  lib,
  packaging,
  pympler,
  typing-extensions,
  wrapt,

  build-native-ext_ ? build-native-ext,
  mypy,
  beartype,
  types-deprecated

}:

buildPythonPackage rec {
  pname = "coredis";
  version = v;

  src = fetchPypi {
    inherit pname version;
    sha256 = hash;
  };
  buildInputs = [] ++ lib.optionals build-native-ext_
    [mypy beartype types-deprecated ];

  propagatedBuildInputs =
    [ async-timeout deprecated typing-extensions packaging pympler wrapt ];

  doCheck = false;

  meta = with lib; {
    description = "Python async client for Redis key-value store";
    homepage = "https://github.com/alisaifee/coredis";
  };
}
