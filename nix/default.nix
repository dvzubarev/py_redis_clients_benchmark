{
  src,
  buildEnv,
  python,
  redis-server,
  coredis,
}:
buildEnv {
  name = "py-redis-clients-bench-env";
  paths = [
    redis-server
    (python.pkgs.callPackage ./bench.nix {inherit src coredis;})


  ];
  pathsToLink=["/bin" "/lib"];
}
