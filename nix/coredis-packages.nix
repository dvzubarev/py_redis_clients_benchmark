final: prev: (
  builtins.mapAttrs(name: value:
    final.callPackage (import ./coredis4.nix value){}
  ) rec{
    coredis402 = {
      v="4.0.2";
      hash = "0ykchpwpd4z108km46yhz2xcwrz42agz8xlf0h7w8i54z25fiv0x";
    };
    coredis421 = {
      v="4.2.1";
      hash = "0i22q8x2wnyzslg2fsh87y6ig369gifvgmhnivz480z490carvfn";
    };
    coredis431 = {
      v="4.4.0";
      hash = "0al1mlfr52iiid2mznhx6clbz28xf7j08iisiaspmkl2jafb7crw";
    };
    coredis460 = {
      v="4.6.0";
      hash = "1b7xpx2niki30ccy49jg01lv1iz3fqnr9q64y7ycd4j5ay8jdgmn";
    };
    coredis460_wo_ext = coredis460 // {build-native-ext=false;};
    coredis = coredis460;

  } //
  builtins.mapAttrs(name: value:
    final.callPackage (import ./coredis3.nix value){}
  ) {
    coredis303 = {
      v="3.0.3";
      hash = "11s7ijdi30wgfdsa0klix65vbz6pwkiw2z33a0cggd9b1nynv60k";
    };

    coredis351 = {
      v="3.5.1";
      hash = "1r44hzs5khmazhx8bbqaj8z5z95v1b6bmjfpczbmbx1hxm63zfzm";
    };
    coredis385 = {
      v="3.8.5";
      hash = "03kxsw90z5ry59hicd9rl25dr2av360sk8vz7aih8jygcqvw23fv";
      #Failed to build extensions in this version
      build-native-ext = false;
    };

    coredis3115 = {
      v="3.11.5";
      hash = "0b49m741s4hsjfa1yz1qyfzcnn1jnif7l7fq7qj0pm2nbm76w685";
    };
  } //
  builtins.mapAttrs(name: value:
    final.callPackage (import ./coredis2.nix value){}
  ) rec{
    coredis231 = {
      v="2.3.1";
      hash = "06vs9fdb03s7b7z5hyv7wk4780fivdbwn75nyf9n65xzsnhldh24";
    };
    coredis231_wo_hiredis = coredis231 // {with-hiredis=false;};
  }
)
