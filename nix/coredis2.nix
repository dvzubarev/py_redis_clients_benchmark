{v, hash, with-hiredis ? true}:
{buildPythonPackage, fetchPypi, lib,
  with-hiredis_ ? with-hiredis, hiredis }:

buildPythonPackage rec {
  pname = "coredis";
  version = v;

  src = fetchPypi {
    inherit pname version;
    sha256 = hash;
  };

  doCheck = false;
  propagatedBuildInputs = [] ++ lib.optionals with-hiredis_ [ hiredis ];

  meta = with lib; {
    description = "Python async client for Redis key-value store";
    homepage = "https://github.com/alisaifee/coredis";
  };
}
