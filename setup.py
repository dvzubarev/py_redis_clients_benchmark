#!/usr/bin/env python3
import os
from setuptools import setup, find_packages


setup(
    name='py-redis-clients-benchmark',
    version=os.environ.get('version', '0'),
    url='',
    author='dvzubarev',
    author_email='denizub@gmail.com',
    license='MIT',
    packages=['py_redis_clients_benchmark'],
    install_requires=[],
    entry_points={
        'console_scripts': [
            'py-redis-clients-benchmark = py_redis_clients_benchmark.bench_cli:main',
        ],
    },
)
