{
  description = "Benchmarking some specifing workflows with some redis clients";

  inputs.nixpkgs.url = "nixpkgs";

  outputs = { self, nixpkgs }:
    let pkgs = import nixpkgs {
          system = "x86_64-linux";
          overlays = [ self.overlay ];
          config = {allowUnfree = true;};
        };
        python-overlay = pyfinal: pyprev: {
          types-deprecated = pyfinal.callPackage ./nix/types-deprecated.nix {};
          py-redis-clients-benchmark = pyfinal.callPackage ./nix/bench.nix {src=self;};
          redis-benchmark-env = pyfinal.callPackage ./nix {src=self;};
        };
        overridePython = py-overlay: final: prev: (
          prev.python310.override (old: {
            self = pkgs.python;
            packageOverrides = final.lib.composeManyExtensions[
              old.packageOverrides or (_: _: { })
              py-overlay
              (import ./nix/coredis-packages.nix)
              (import ./nix/bench-variants.nix {src=self;})
            ];
          })
        );
    in {
      overlay = final: prev: {
        python = overridePython python-overlay final prev;
        redis-server = prev.redis;
      };

      packages.x86_64-linux = {
        inherit (pkgs)
          python;
      };

      defaultPackage.x86_64-linux = pkgs.python.pkgs.redis-benchmark-env;

      devShells.x86_64-linux =
        let pypkgs = pkgs.python.pkgs;
        in
          builtins.mapAttrs(name: value:
            pkgs.mkShell {
              inputsFrom = [(pypkgs.callPackage ./nix/bench.nix ({src=self;} // value))];
              buildInputs = [
                pkgs.redis-server
                pkgs.nodePackages.pyright
                pkgs.nodePackages.bash-language-server
                pkgs.shellcheck
                pypkgs.pylint
                pypkgs.black
                # pypkgs.debugpy
                pypkgs.ipykernel
              ];

              # shellHook='''';
            }
          ){
            dev_coredis231 = {coredis=pypkgs.coredis231;};
            default = {coredis=pypkgs.coredis460;};
          };

    };

}
